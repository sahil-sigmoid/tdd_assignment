

class Monotonic:
    def isMonotonic(A):
        a=False
        b=False
        for i in range(len(A)-1):
            if(type(i)==type(1)):
                a=True
            if(type(i)==type("a")):
                b=True
        if(a and b):
            raise Exception("Bad Input")

        return (all(A[i] <= A[i + 1] for i in range(len(A) - 1)) or
                all(A[i] >= A[i + 1] for i in range(len(A) - 1)))


    def readFromFile(self, file_name):
        infile = open(file_name, "r")
        line = infile.readline()
        return line



