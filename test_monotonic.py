
from monotonic import Monotonic
import pytest
import os.path
from unittest.mock import MagicMock

class TestMonotonic:
    @pytest.fixture()
    def monotonicFun(self):
        monotonicObj = Monotonic()
        return monotonicObj

    @classmethod
    def setup_class(cls):
        print("\nMonotonic increasing or decreasing")

    @classmethod
    def teardown_class(cls):
        print("\n* End *")

    def test_input_file(self):
        assert os.path.exists("input.txt") == 1

    @pytest.mark.parametrize("test_input,expected", [([1,2,3], True),([1,1,2,2,2,3], True),([4,3,2,1],True),([4,3,2,1,5],False)])
    def test_eval(self,test_input, expected):
        assert Monotonic.isMonotonic(test_input) == expected

    def test_exception_with_bad_input(self, monotonicFun):
        with pytest.raises(Exception):
            monotonicFun.isMonotonic([1,'a',2])

    def test_returns_correct_output(self,monotonicFun, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value=1)
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = monotonicFun.readFromFile("/Users/sahilseli/Sigmoid/TDD/Assignment/input.txt")
        mock_open.assert_called_once_with("/Users/sahilseli/Sigmoid/TDD/Assignment/input.txt", "r")
        assert result == 1




